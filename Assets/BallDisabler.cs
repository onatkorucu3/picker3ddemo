﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallDisabler : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.GetComponent<Transform>().position.y < -100) {
            gameObject.SetActive(false);
        }


    }

    void OnTriggerEnter(Collision collision)
    {
        if (collision.collider.gameObject.tag.CompareTo("ScoreCounter")==0 )
        {
            gameObject.SetActive(false);
        }
    }
}
