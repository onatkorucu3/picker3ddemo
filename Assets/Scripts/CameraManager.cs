﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    private Transform targetToLook;
    private Vector3 initialFollowDistance;
    private Vector3 moveVector;
    private float transition = 0.0f;
    private float animationDuration = 2.0f;
    private Vector3 animationOffset = new Vector3(0,5,5);

    // Start is called before the first frame update
    void Start()
    {
        targetToLook = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        initialFollowDistance = transform.position - targetToLook.position;
    }

    void LateUpdate()
    {
        moveVector = targetToLook.position + initialFollowDistance;

        moveVector.x = 0;

        if (transition > 1.0f)
        {
            transform.position = moveVector;
        }
        else {
            //Camera movement from above to ground level that happens at the start of the game.
            transform.position = Vector3.Lerp(moveVector + animationOffset,moveVector,transition);
            transition += Time.deltaTime * 1 / animationDuration;
            transform.LookAt(targetToLook.position + Vector3.up);
        }

    }
}
