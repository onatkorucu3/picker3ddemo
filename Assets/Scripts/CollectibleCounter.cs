﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro; // Add the TextMesh Pro namespace to access the various functions.

public class CollectibleCounter : MonoBehaviour, IPooledObject
{
    public TextMesh pointText;

    public int goalCount;

    private TextMeshProUGUI textMeshPro;

    [SerializeField]
    private int points;

    void Awake()
    {
        pointText = GetComponentInChildren<TextMesh>();
        textMeshPro = FindObjectOfType<TextMeshProUGUI>();
        goalCount = 30; //TODO Change
    }

    public void OnObjectSpawn()
    {
        points = 0;
        textMeshPro.SetText(points.ToString() + "/" + goalCount);
    }

    private void OnTriggerEnter(Collider other)
    {
        points++;
        textMeshPro.SetText(points.ToString() + "/" + goalCount);
    }

}
