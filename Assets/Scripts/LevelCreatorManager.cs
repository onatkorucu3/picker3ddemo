﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LevelCreatorManager : MonoBehaviour
{
    ObjectPooler objectPooler;


    // Start is called before the first frame update
    void Start()
    {
        objectPooler = ObjectPooler.Instance;
        CreateLevel();
        objectPooler.SpawnFromPool("myTag",transform.position, Quaternion.identity);
    }

    private void CreateLevel()
    {
        string[] sectionNameList = ReadLevelText();

        foreach (string sectionName in sectionNameList) {
            CreateSection(sectionName);
        }

    }

    private string[] ReadLevelText()
    {
        TextAsset bindData = Resources.Load("Level1") as TextAsset;

        string data = bindData.text.Replace(Environment.NewLine, string.Empty);

        return data.Split('-');

    }
    private void CreateSection(string sectionName)
    {
        string[] sectionTextLines = ReadSectionText(sectionName);

        string metaDataLine = sectionTextLines[0];
        string[] metaDataList = metaDataLine.Split(',');

        string materialOfPlatform = metaDataList[0];
        int lengthOfPlatform = Int32.Parse(metaDataList[1]);
        int neededBallNumberToPass = Int32.Parse(metaDataList[2]);
        int totalBallNumber = Int32.Parse(metaDataList[3]);
        //int ballLineCount = Int32.Parse(metaDataList[4]); //TODO delete
        int ballLineCount = sectionTextLines.Length - 1; //TODO delete

        Vector3 positionOfPlatform = new Vector3();

        for (int i = 1; i<sectionTextLines.Length; i++) 
        {
            char[] lineWithBalls = sectionTextLines[i].ToCharArray();
            PutBallsOnPlatform(lineWithBalls, positionOfPlatform, lengthOfPlatform, ballLineCount, i);

        }

    }

    private void PutBallsOnPlatform(char[] lineWithBalls, Vector3 positionOfPlatform, int lengthOfPlatform, int ballLineCount, int lineIndex)
    { 
        
    }


    private string[] ReadSectionText(string sectionName)
    {
        TextAsset bindData = Resources.Load(sectionName) as TextAsset;
        string data = bindData.text.Replace(Environment.NewLine, string.Empty);
        return data.Split('-');
    }
}
