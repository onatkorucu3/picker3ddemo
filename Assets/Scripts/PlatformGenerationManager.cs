﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformGenerationManager : MonoBehaviour
{
    public GameObject platformToCreate;
    public Transform generationPoint;
    public float distanceBetween;
    //private float platformLength;
    //public Renderer renderer;

    Vector3 lastPlatformPosition;

    GameObject newPlatform;

    [SerializeField]
    public ObjectPooler objectPooler;

    Bounds bounds;
    float platformLength;

    // Start is called before the first frame update
    void Start()
    {
        objectPooler = ObjectPooler.Instance;

        bounds = platformToCreate.GetComponent<Renderer>().bounds;

        /*foreach (Transform child in platformToCreate.transform)
        {
            bounds.Encapsulate(child.gameObject.GetComponent<Renderer>().bounds);
        }
        platformLength = platformToCreate.GetComponent<Renderer>().bounds.size.z;*/

        platformLength = 130f;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.z - lastPlatformPosition.z > platformLength)
        {
            lastPlatformPosition = transform.position;

            newPlatform = objectPooler.SpawnFromPool("Level", transform.position, transform.rotation);

            /*GameObject thing = GameObject.Find("Section11");

            Bounds bounds = thing.GetComponent<Renderer>().bounds;
            foreach (Transform child in thing.transform)
            {
                bounds.Encapsulate(child.gameObject.GetComponent<Renderer>().bounds);
            }

            Debug.Log("OOOOOOO"+bounds);  //prints out correct bounds*/

        }

    }
}
