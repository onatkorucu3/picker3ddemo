﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformGeneratorPositionManager : MonoBehaviour
{
    private Transform targetToLead;
    private float initialFollowDistance;
    private Vector3 moveVector;

    // Start is called before the first frame update
    void Start()
    {
        targetToLead = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        initialFollowDistance = transform.position.z - targetToLead.position.z;
    }

    // Update is called once per frame
    void Update()
    {
        moveVector.z = targetToLead.position.z + initialFollowDistance;

        //moveVector.y = targetToLead.position.y;
        // moveVector.y = Mathf.Clamp(moveVector.y,3,5);

        transform.position = moveVector;
    }
}

