﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatforrmDisablerManager : MonoBehaviour
{
    public GameObject mainCamera;
    private float platformLength;

    void Start() 
    {
        platformLength = gameObject.GetComponent<Renderer>().bounds.size.z;
    }

    // OnEnable is called each time this object is pooled.
    void OnEnable()
    {
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
    }

    // Update is called once per frame
    void Update()
    {
        //We disable the object when it is out of sight, so we can pool it later when needed.
        if (transform.position.z + platformLength/2 < mainCamera.transform.position.z) {
            gameObject.SetActive(false);
        }
    }
}
