﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    private Vector3 moveVector;
    public CharacterController controller;

    private Rigidbody rb;

    Vector3 newPosition;

    [SerializeField]
    public float speed = 5.0f;
    private float animationDuration = 2.0f; //TODO get from other script



    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController>();
        rb = GetComponent<Rigidbody>();


        moveVector = Vector3.zero;
        moveVector.z = speed;


    }

    private void Update()
    {
        moveVector.x = Input.GetAxisRaw("Horizontal") * speed;
    }

    //FixedUpdate is called at a fixed interval and is independent of frame rate. Put physics code here.
    void FixedUpdate()
    {
        //call on start and between levels
        if (Time.time < animationDuration) {
            //controller.Move(Vector3.forward * speed * Time.deltaTime);
            rb.MovePosition(transform.position + Vector3.forward * speed * Time.deltaTime);

            return;
        }

        newPosition = transform.position + moveVector * Time.deltaTime;
        newPosition.x = Mathf.Clamp(newPosition.x, -9.1f, 9.3f);

        rb.MovePosition(newPosition);
    }
}